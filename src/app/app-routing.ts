import { Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

export const APP_ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'books',
    loadComponent: () => import('./pages/books/books.component')
        .then(mod => mod.BooksComponent)
  },
  {
    path: 'admin',
    loadChildren: () => import('./pages/admin/admin-routing')
        .then(mod => mod.ADMIN_ROUTES)
  }
];
