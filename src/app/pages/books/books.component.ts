import { CommonModule } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTable, MatTableModule } from '@angular/material/table';
import { BooksDatasource, TabItem } from './books-datasource';

@Component({
  standalone: true,
  selector: 'books-page',
  templateUrl: './books.component.html',
  imports: [
    CommonModule, MatTableModule, MatPaginatorModule, MatSortModule
  ],
  styleUrls: ['books.component.scss']
})
export class BooksComponent {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<TabItem>;
  dataSource: BooksDatasource;

  displayedColumns = ['id', 'name'];

  constructor() {
    this.dataSource = new BooksDatasource();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
