import { Component } from '@angular/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@Component({
  standalone: true,
  selector: 'admin-dashboard-page',
  imports: [
    MatProgressBarModule
  ],
  templateUrl: './admin-dashboard.component.html'
})
export class AdminDashboardComponent {

}
