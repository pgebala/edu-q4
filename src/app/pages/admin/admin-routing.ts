import { Route } from '@angular/router';
import { AdminDashboardComponent } from './dashboard/admin-dashboard.component';
import { AdminHomeComponent } from './home/admin-home.component';

export const ADMIN_ROUTES: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  { path: 'home', component: AdminHomeComponent },
  { path: 'dashboard', component: AdminDashboardComponent }
];
