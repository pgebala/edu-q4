import { Component } from '@angular/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@Component({
  standalone: true,
  selector: 'admin-home-page',
  imports: [
    MatProgressBarModule
  ],
  templateUrl: './admin-home.component.html'
})
export class AdminHomeComponent {

}
